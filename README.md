# TS BehaviorTree

#### 项目介绍
使用TypeScript的Generator实现的行为树(BehaviorTree)

#### 使用说明


    注: 添加多个行为树节点要注意节点的引用,以免误把同一个节点添加到多棵树中!!!
    注: 行为树支持嵌套
 
    用法示例:
    ```
    import $rootNode = FBT.$rootNode;
    import $sequence = FBT.$sequence;
    import $do = FBT.$do;
    import $doT = FBT.$doT;
    import $condition = FBT.$condition;
    import $waitNode = FBT.$waitNode;
    import $parallel = FBT.$parallel;
    import $loop = FBT.$loop;

    const rootNode = $rootNode(
        $sequence("All",
            $do("isOwnerNoDeath", AutoFightComponent.isOwnerNoDeath),               /**判断目标是否死亡 */
            $do("hasSkilling", AutoFightComponent.hasSkilling),                     /**确认是否正在释放某些技能或者现在处于硬直状态 */
            $do("findMonsterOfClosedMe", AutoFightComponent.findMonsterOfClosedMe), /**查找并锁定怪物 */
            $do("randomSkillId", (state) => this.randomSkillId(state)),             /** 随机一个技能准备释放 */
            $selector("do selector",
                $do("decideSkillDistanceIsExceed",
                    (state) => this.decideSkillDistanceIsExceed(state)),    /** 判断释放的技能是否能达到目标点*/
                $do("findRoadToPoint", AutoFightComponent.findRoadToPoint), /**自动寻路到某一点 */
                $parallel("Loop decide condition", 1, 2, /**需要如果死亡 、没有死亡且到达目标点则需要返回主节点 */
                    $do("isDeadHeat", AutoFightComponent.isDeadHeat),                       /**判断是否达到了目标点 */
                    $do("updateTargetPoint", AutoFightComponent.updateFindRoadTargetPoint), /**更新目标位置是否改变 */
                    $do("isTargetNoDeath", AutoFightComponent.isTargetNoDeath)              /**判断主角是否死亡 */
                )
            ),
            $do("skillAttack", AutoFightComponent.playerFireSkillToMonster) /**开始释放技能攻击目标 */
        )
   
    ```

    每帧调用 rootNode.tick(deltaTime);

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
